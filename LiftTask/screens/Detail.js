
import React, { useState, useEffect } from 'react';
import { SafeAreaView, StyleSheet, View, Text, StatusBar } from 'react-native';

export default DetailScreen = ({ route, navigation }) => {

    const { data } = route.params;
    const [result, setResult] = useState('0');

    useEffect(() => {

        let executionResult = ""
        for (let i = 0; i < data.length; i++) {
            executionResult += data[i].floorNumber + (i != data.length - 1 ? " -> " : "")
        }
        setResult(executionResult);
    })

    return (
        <SafeAreaView style={[style.safeAreaFrame, { alignItems: 'center', justifyContent: 'center', alignContent: 'center' }]}>
            <Text style={{ fontSize: 20, fontWeight: '400', color: 'white', alignContent: 'center' }}>{'Order Of Execution Is: \n' + result}</Text>
        </SafeAreaView>
    )
}

const style = StyleSheet.create({
    safeAreaFrame: {
        flex: 1,
        backgroundColor: 'gray',
        marginTop: StatusBar.currentHeight || 0,
        flexDirection: 'column'
    }
})