
import React, { useState } from 'react';
import { SafeAreaView, StyleSheet, View, Text, FlatList, StatusBar, TouchableOpacity } from 'react-native';
import { CalculationMethod } from '../HelperFunctions';

export default Home = ({ navigation }) => {

    const [selectedLiftNumbers, setSelectedLiftNumbers] = useState([]);
    const [selectedObj, setSelectedObj] = useState({ floorNumber: 0, isUp: true })

    actionMethod = (index, isUp) => e => {
        console.log('index: ', index);
        let obj = {
            floorNumber: index,
            isUp: isUp
        }
        let selectedNumbers = selectedLiftNumbers || [];
        selectedNumbers.push(obj);
        setSelectedLiftNumbers(selectedNumbers);
    }

    actionForStart = () => {

        let selectedNumbers = selectedLiftNumbers || [];
        if (selectedNumbers.length == 0) {
            alert('Please select floor');
            return;
        }


        let finalOutputArray = CalculationMethod(selectedNumbers);
        finalOutputArray.map((data, index) => {
            setTimeout(() => {
                setSelectedObj(data);
            }, 1000 * index);
        })

        setTimeout(() => {
            navigation.push('Detail', { data: finalOutputArray });
        }, 1000 * finalOutputArray.length);
    }

    const renderItem = ({ item, index }) => {

        if (index == 7) {
            return (<View style={[style.rowFrame, { padding: 10, alignItems: 'center', justifyContent: 'center' }]}>
                <TouchableOpacity style={{ width: '80%', height: 70, backgroundColor: 'white', padding: 0, alignItems: 'center', justifyContent: 'center' }} onPress={actionForStart}>
                    <RowView count={"Start Run"} backgroundColor="yellow" arrowText={''} viewStyle={{ width: '60%' }} />
                </TouchableOpacity>
            </View>)
        }

        let backgroundColor = 'lightgray';
        if (selectedObj.floorNumber == item.floorNumber) {
            backgroundColor = "green";
        }

        let rowView = <TouchableOpacity onPress={actionMethod(item.floorNumber, true)}><RowView count={item.title} backgroundColor="white" arrowText={'Up'} /></TouchableOpacity>
        if (index == 0 || index == 6) {
            rowView = null
        }

        return (
            <View style={style.rowFrame}>
                <View style={{ backgroundColor: backgroundColor, width: 75, height: 75 }}></View>
                <Text style={style.textFrame}>{''}</Text>
                <View style={{ justifyContent: 'space-around', backgroundColor: 'white', flexDirection: 'row', width: '80%', alignItems: 'center' }}>
                    {rowView}
                    <TouchableOpacity onPress={actionMethod(item.floorNumber, false)}>
                        <RowView count={item.title} backgroundColor="white" arrowText={index == 6 ? 'Up' : 'Down'} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    return (
        <SafeAreaView style={style.safeAreaFrame}>
            <FlatList
                data={liftDataArray}
                renderItem={renderItem}
                keyExtractor={item => item.id}
            />
        </SafeAreaView>
    )
}

const style = StyleSheet.create({
    safeAreaFrame: {
        flex: 1,
        backgroundColor: 'gray',
        marginTop: StatusBar.currentHeight || 0,
        flexDirection: 'column'
    },
    rowFrame: {
        height: 75,
        paddingLeft: 5,
        backgroundColor: 'white',
        marginVertical: 8,
        marginHorizontal: 16,
        flexDirection: 'row',
        alignItems: 'stretch'
    },
    textFrame: {
        marginLeft: 2,
        height: 75,
        width: 1,
        backgroundColor: 'gray'
    }
})

const RowView = ({ count, backgroundColor, arrowText, isGround = false, viewStyle }) => {
    return (<View style={[{ padding: 10, height: 60, alignItems: 'center', justifyContent: 'center', backgroundColor: backgroundColor, borderColor: 'green', borderWidth: 1 }, viewStyle]}>
        <Text>{arrowText}</Text>
        <Text>{count}</Text>
    </View>)
}

const liftDataArray = [
    {
        id: '1',
        title: 'Lift Floor 6',
        floorNumber: 6
    },
    {
        id: '2',
        title: 'Lift Floor 5',
        floorNumber: 5
    },
    {
        id: '3',
        title: 'Lift Floor 4',
        floorNumber: 4
    },
    {
        id: '4',
        title: 'Lift Floor 3',
        floorNumber: 3
    },
    {
        id: '5',
        title: 'Lift Floor 2',
        floorNumber: 2
    },
    {
        id: '6',
        title: 'Lift Floor 1',
        floorNumber: 1
    },
    {
        id: '7',
        title: 'Ground Floor',
        floorNumber: 0
    },
    {
        id: '8',
        title: '',
        floorNumber: -1
    },
];