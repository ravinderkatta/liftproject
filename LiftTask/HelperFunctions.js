
export const CalculationMethod = dataArray => {

    let downArray = [];
    let upArray = [];

    // Separate the up & down
    for (var i = 0; i < dataArray.length; i++) {
        if (dataArray[i].isUp) {
            upArray.push(dataArray[i])
        } else {
            downArray.push(dataArray[i])
        }
    }

    let finalOutputArray = [];
    // Sort up lift numbers
    for (var k = 0; k < upArray.length; k++) {

        if (k + 1 == upArray.length) {
            finalOutputArray.push(upArray[k]);
            break;
        }

        let currentFloorNumber = upArray[k].floorNumber;
        for (var i = k + 1; i < upArray.length; i++) {
            if (currentFloorNumber > upArray[i].floorNumber) {
                let currentFloorObj = upArray[k];
                upArray[k] = upArray[i];
                upArray[i] = currentFloorObj;
            }
        }
        finalOutputArray.push(upArray[k]);
    }

    // Sort down lift numbers
    for (var j = 0; j < downArray.length; j++) {

        if (j + 1 == downArray.length) {
            finalOutputArray.push(downArray[j]);
            break;
        }

        let currentFloorNumber = downArray[j].floorNumber;

        for (var i = j + 1; i < downArray.length; i++) {
            if (downArray[i].floorNumber > currentFloorNumber) {
                let currentFloorObj = downArray[j];
                downArray[j] = downArray[i];
                downArray[i] = currentFloorObj;
            }
        }
        finalOutputArray.push(downArray[j]);
    }

    // console.log('downArray: ', downArray);
    // console.log('upArray: ', upArray);
    // console.log('final: ', finalOutputArray);

    return finalOutputArray;
}