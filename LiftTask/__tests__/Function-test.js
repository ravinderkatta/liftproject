
import 'react-native';
import React from 'react';
import { CalculationMethod } from '../HelperFunctions';

import renderer from 'react-test-renderer';

const matchData = [{ floorNumber: 3, isUp: true }, { floorNumber: 4, isUp: false }, { floorNumber: 2, isUp: false }]

it('data should be sort and match object', () => {
    let dataObj = [
        {
            floorNumber: 4,
            isUp: false
        }, {
            floorNumber: 3,
            isUp: true
        }, {
            floorNumber: 2,
            isUp: false
        }]
    expect(CalculationMethod(dataObj)).toEqual(matchData);
})